import { Row, Col, Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function CourseCard({ courseProp }) {
  const { name, description, price, _id } = courseProp;

  return (
    <Row className="mt-3 mb-3">
      <Col>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price</Card.Subtitle>
            <Card.Text>PHP {price}</Card.Text>
            {/* <Card.Text>Enrollees: {count}</Card.Text>
            <Card.Text>Seats Available: {seats}</Card.Text> */}
            <Button className="bg-primary" as={Link} to={`/courses/${_id}`}>
              Details
            </Button>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
