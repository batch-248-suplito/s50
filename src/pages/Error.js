// import { useEffect } from "react";
// import Banner from "../components/Banner";
// import { Container } from "react-bootstrap";
// import Swal from "sweetalert2";

// export default function Error() {
//   const handleClick = () => {
//     Swal.fire({
//       icon: "error",
//       title: "Oops!",
//       text: "Something went wrong!",
//     });
//   };

//   useEffect(() => {
//     handleClick();
//   }, []);

//   return (
//     <Container className="text-center mt-5">
//       {handleClick}
//       <Banner
//         title="Error 404: Page not found"
//         description="The page you are looking for doesn't exist."
//         buttonText="Go back home"
//         buttonLink="/"
//       />
//     </Container>
//   );
// }

import Banner from "../components/Banner";

export default function Error() {
  const data = {
    title: "404 - Page Not Found",
    content: "The page you are looking for cannot be found",
    destination: "/",
    label: "Back to Home",
  };

  return <Banner data={data} />;
}
