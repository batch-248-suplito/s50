//import courses from "../data/courses";
import { useEffect, useState, useContext } from "react";
import CourseCard from "../components/CourseCard";
import UserContext from "../UserContext";

export default function Courses() {
  const [courses, setCourses] = useState([]);
  const { user } = useContext(UserContext);

  useEffect(() => {
    fetch("http://localhost:4000/courses")
      .then((res) => res.json())
      .then((data) => {
        const courseArr = data.map((course) => {
          return <CourseCard courseProp={course} key={course.id} />;
        });
        setCourses(courseArr);
      });
  });

  return (
    <>
      <h1>Courses</h1>
      {courses}
    </>
  );
}
